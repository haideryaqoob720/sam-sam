const db = require("../models");
const User = db.user;
var path = require('path')

exports.index = function (req, res) {
    message = '';
    if(req.method == "POST"){
        let currentDir = __dirname;
        desiredPath = currentDir.substr(0,currentDir.lastIndexOf('/'))
        if (!req.files)
            return res.status(400).send('No files were uploaded.');

        var file = req.files.uploaded_image;
        var img_name = file.name;
        var nameWithoutExtension = img_name.substr(0, img_name.lastIndexOf('.'));
        let uploadPath;
        var imgNameDb = nameWithoutExtension + Date.now() + path.extname(img_name)
        uploadPath =  desiredPath + '/public/images/' + imgNameDb

        if (file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {

            file.mv(uploadPath, function (err) {

                if (err){
                    console.log("error: ", err);
                    return res.status(500).send(err);
                } else {
                const userProfile = {
                    firstName: req.body.first_name,
                    secondName: req.body.last_name,
                    imageName: imgNameDb
                };

                // Save userProfile in the database
                console.log("param", req.query.userId)
                console.log("profile data", req.body)
                User.update(userProfile,
                    {
                        where: { id: req.query.userId }
                    }
                    )
                    .then(data => {
                        console.log("Profile inserted in db: ", data)
                        console.log("Image Uploaded successfully")
                        res.send('File uploaded!');
                    })
                    .catch(err => {
                        res.status(500).send({
                            message:
                                err.message || "Some error occurred while creating the profile."
                        });
                    });
                }
            });
        } else {
            message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
            res.render('index.ejs', { message: message });
        }
    }else {
        res.render('index');
    }

};

exports.profile = async function (req, res) {
    var user = await User.findOne({ where: { id: req.query.id } });
        res.send({id: user.id, firstName: user.firstName, lastName: user.lastName, imageName: user.imageName});
};